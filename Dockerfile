FROM openjdk:8

RUN apt update

RUN apt install -y software-properties-common

RUN add-apt-repository ppa:deadsnakes/ppa

RUN apt install -y python3-pip

RUN wget -O /usr/src/spark-3.2.0-bin-hadoop2.7.tgz https://dlcdn.apache.org/spark/spark-3.2.0/spark-3.2.0-bin-hadoop2.7.tgz

RUN tar zxvf /usr/src/spark-3.2.0-bin-hadoop2.7.tgz --directory /usr/src

COPY requirements.txt /usr/src/myapp/requirements.txt
WORKDIR /usr/src/myapp
RUN pip install -r requirements.txt

COPY spark /usr/src/myapp/spark
COPY jobs /usr/src/myapp/jobs
COPY etl /usr/src/myapp/etl
COPY test /usr/src/myapp/test

RUN chmod +x /usr/src/myapp/etl/start_etl.sh

ENV PYTHONPATH=/usr/src/myapp/

CMD ["/bin/bash"]