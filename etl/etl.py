"""
This module contains ETL sequence
"""
from argparse import ArgumentParser

import os

from jobs.ingest import Ingester
from jobs import length_dist, time_gross_dist, time_net_dist

if __name__ == "__main__":

    def parse_arg_command():
        parser = ArgumentParser()
        parser.add_argument("--drop_partition_if_exists", default="yes", required=False, help="yes/no flag to drop ingest date partition if it exists")
        parser.add_argument("--input_dir", default="", required=False, help="landing directory")
        parser.add_argument("--output_dir", default="", required=False, help="output directory")
        parser.add_argument("--spark_wh_dir", default="", required=False, help="Spark warehouse directory")
        return parser.parse_args()

    args = parse_arg_command()
    input_dir, output_dir, spark_wh_dir, drop_partition_if_exists = \
        args.input_dir, args.output_dir, args.spark_wh_dir, args.drop_partition_if_exists == 'yes'

    if not (os.path.isdir(input_dir) and os.path.exists(os.path.join(input_dir))):
        print(f'the "{input_dir}" input folder doesn\'t exist')
        exit(-1)
    if not (os.path.isdir(output_dir) and os.path.exists(os.path.join(output_dir))):
        print(f'the "{input_dir}" output folder doesn\'t exist')
        exit(-1)
    if not spark_wh_dir:
        print('the spark warehouse folder name is empty ')
        exit(-1)

    try:
        # ingest data
        ingest_obj = Ingester(input_dir, spark_wh_dir, drop_partition_if_exists)
        ingest_obj.run()

        # calculate and output Length Distribution
        length_dist.calc_length_dist(spark_wh_dir, output_dir)

        # calculate and output Length Distribution
        time_gross_dist.calc_time_gross_dist(spark_wh_dir, output_dir)

        # calculate and output Length Distribution
        time_net_dist.calc_time_net_dist(spark_wh_dir, output_dir)

    except Exception as err:
        err = 'Error has occurred {0}'.format(str(err))
        print(err)
        exit(-1)
