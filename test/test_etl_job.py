"""
test_etl_job.py
~~~~~~~~~~~~~~~

This is example of unit test for the steps of the ETL
"""
import os, shutil

import datetime
import unittest
from pathlib import Path

import spark.spark as spark
from jobs.ingest import Ingester
from jobs import length_dist, time_gross_dist, time_net_dist

import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

class SparkETLTests(unittest.TestCase):

    def setUp(self):
        """Start Spark, path to test data
        """
        project_path = Path(__file__).parent.parent
        self.test_data_input_dir = f"{project_path}\\test\\data"
        self.test_data_output_dir = f"{project_path}\\output"
        self.spark_sql_warehouse_dir = f"{project_path}\\spark_wh"
        self.drop_partition_if_exists = True

        self.test_data_expected_count = 47881

        self.session = spark.get_or_create_session(self.spark_sql_warehouse_dir)

        self.test_date = datetime.date.today()

    def tearDown(self):
        """Stop Spark
        """
        self.session.stop()

    def test_etl (self):
        # ingest data
        ingest_obj = Ingester(self.test_data_input_dir, self.spark_sql_warehouse_dir, self.drop_partition_if_exists)
        actual_count = ingest_obj.run()
        self.assertEqual(actual_count, self.test_data_expected_count )

        # clean output folder content
        for filename in os.listdir(self.test_data_output_dir):
            file_path = os.path.join(self.test_data_output_dir, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))

        expected_output_file_name = self.test_data_output_dir + f'/length_dist_{self.test_date}.csv'
        # calculate and output Length Distribution
        length_dist.calc_length_dist(self.spark_sql_warehouse_dir, self.test_data_output_dir)
        self.assertTrue( os.path.isdir(expected_output_file_name) )

        expected_output_file_name = self.test_data_output_dir + f'/time_gross_dist_{self.test_date}.csv'
        # calculate and output Length Distribution
        time_gross_dist.calc_time_gross_dist(self.spark_sql_warehouse_dir, self.test_data_output_dir)
        self.assertTrue( os.path.isdir(expected_output_file_name) )

        expected_output_file_name = self.test_data_output_dir + f'/time_net_dist_{self.test_date}.csv'
        # calculate and output Length Distribution
        time_net_dist.calc_time_net_dist(self.spark_sql_warehouse_dir, self.test_data_output_dir)
        self.assertTrue( os.path.isdir(expected_output_file_name) )

if __name__ == '__main__':
    unittest.main()