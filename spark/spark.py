from pyspark.sql import SparkSession

def get_or_create_session(spark_sql_warehouse_dir):
    session = SparkSession \
        .builder \
        .appName("geofiles") \
        .config("spark.sql.warehouse.dir", spark_sql_warehouse_dir) \
        .config("spark.hadoop.hive.metastore.warehouse.dir", spark_sql_warehouse_dir) \
        .enableHiveSupport() \
        .getOrCreate()
        # .config("spark.driver.memory", "2g") \
        # .config("spark.executor.memory", "6g") \
        # .config("spark.executor.pyspark.memory", "4g") \
    return session
