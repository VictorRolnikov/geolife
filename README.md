## Geolife Project Structure

The basic project structure is as follows:

```bash
.
├── Dockerfile
├── README.md
├── etl
│   ├── etl.py
│   └── start_etl.sh
├── jobs
│   ├── __init__.py
│   ├── __pycache__
│   ├── ingest.py
│   ├── length_dist.py
│   ├── time_gross_dist.py
│   └── time_net_dist.py
├── output
│   ├── length_dist_2021-10-24.csv
│   ├── time_gross_dist_2021-10-25.csv
│   └── time_net_dist_2021-10-25.csv
├── requirements.txt
├── spark
│   └── spark.py
└── test
    ├── __init__.py
    ├── __pycache__
    ├── data
    └── test_etl_job.py
```

## Structure of an ETL Job

### ingest.py  
The main steps :
1) parsing and validation input arguments 
2) Get or create Spark session 
3) Create DataFrame schema for geolife plt files
4) Create if not exist Spark tables (staging and main) 
5) drop existing partition by ingest date 
6) ingest geofile data from the input directory into the staging "geolife_stg" table, 
7) insert enriched and transformed data into "geolife" table, truncate staging table  


Example of the staging "geolife_stg" table, with added columns : 
- lat_rad, lon_rad : converted to radians
- time_stamp      :  combined date & time as one column
- ingest_date     : ingest processing date, used for partitions
```
+----+-------+--------+---------+----------+----------+---------+-------------------+-------------------+
|user|dataset|altitude| latitude| longitude|   lat_rad|  lon_rad|         time_stamp|        ingest_date|
+----+-------+--------+---------+----------+----------+---------+-------------------+-------------------+
| 001|      0|      90|40.015026| 116.31127|0.69839394|2.0300148|2008-10-25 23:40:13|2021-10-24 00:00:00|
| 001|      0|      56|40.015125| 116.31117|0.69839567|2.0300128|2008-10-25 23:40:14|2021-10-24 00:00:00|
| 001|      0|      56|40.015125| 116.31117|0.69839567|2.0300128|2008-10-25 23:40:17|2021-10-24 00:00:00|
| 001|      0|      50|40.015118| 116.31117|0.69839555|2.0300128|2008-10-25 23:40:19|2021-10-24 00:00:00|
| 001|      0|     100|40.015064| 116.31123| 0.6983946| 2.030014|2008-10-25 23:40:22|2021-10-24 00:00:00|
| 001|      0|     116|40.015083|116.311226|0.69839495|2.0300138|2008-10-25 23:40:27|2021-10-24 00:00:00|
| 001|      0|     101|40.015125| 116.31119|0.69839567| 2.030013|2008-10-25 23:40:32|2021-10-24 00:00:00|
+----+-------+--------+---------+----------+----------+---------+-------------------+-------------------+
```

Example of the main "geolife" table, with added columns : 
- lat_rad_prev, lon_rad_prev : previous position's coords of user
- time_stamp_prev            : previous sampling time_stamp 
```
+----+-------+--------+---------+----------+----------+---------+------------+------------+-------------------+-------------------+-------------------+
|user|dataset|altitude| latitude| longitude|   lat_rad|  lon_rad|lat_rad_prev|lon_rad_prev|         time_stamp|    time_stamp_prev|        ingest_date|
+----+-------+--------+---------+----------+----------+---------+------------+------------+-------------------+-------------------+-------------------+
| 000|      0|     490| 40.01258|116.297264|0.69835126|2.0297701|   0.6983515|   2.0297704|2008-10-28 00:38:36|2008-10-28 00:38:31|2021-10-24 00:00:00|
| 000|      0|     490| 40.01245| 116.29718|  0.698349|2.0297687|  0.69835126|   2.0297701|2008-10-28 00:38:41|2008-10-28 00:38:36|2021-10-24 00:00:00|
| 000|      0|     490|40.012398|116.297134| 0.6983481| 2.029768|    0.698349|   2.0297687|2008-10-28 00:38:46|2008-10-28 00:38:41|2021-10-24 00:00:00|
| 000|      0|     490|40.012398|116.297134| 0.6983481| 2.029768|   0.6983481|    2.029768|2008-10-28 00:38:49|2008-10-28 00:38:46|2021-10-24 00:00:00|
| 000|      0|     490| 40.01239|116.297134| 0.6983479| 2.029768|   0.6983481|    2.029768|2008-10-28 00:38:51|2008-10-28 00:38:49|2021-10-24 00:00:00|
| 000|      0|     491|40.012394|116.297165|0.69834805|2.0297685|   0.6983479|    2.029768|2008-10-28 00:38:56|2008-10-28 00:38:51|2021-10-24 00:00:00|
+----+-------+--------+---------+----------+----------+---------+------------+------------+-------------------+-------------------+-------------------+
```


### length_dist.py

Reads the ingested data from the "geolife" table located in Spark warehouse dir (parametrized) 
Calculates a dustribution of trip (“trajectory”) lengths in km
Outputs the results to the output directory (parametrized) in csv format

```
├── output
   └── length_dist_2021-10-24.csv
      └── part-00000-83024ca1-61dc-45d3-8868-72d79bf25fe4-c000.csv
```
   
the CSV file contains the following structure :
```
    range_km,percentage_of_total
    < 5 km,14.62
    5km - 20km,14.29
    20km - 100km,28.57
    >= 100km,42.86   
```

### time_gross_dist.py 

Reads the ingested data from the "geolife" table located in Spark warehouse dir (parametrized) 
Calculates the distribution of trip duration time - gross, including stops.
Outputs the results to the output directory (parametrized) in csv format
 
```
├── output
   └── time_gross_dist_2021-10-24.csv
      └── part-00000-c4f9ca61-14e1-4279-be57-295e61e4eca4-c000.csv
```
   
the CSV file contains the following structure :
```
    range_minutes,percentage_of_total
    >= 2hours,100.0
```
 
### time_net_dist.py 
Reads the ingested data from the "geolife" table located in Spark warehouse dir (parametrized) 
Calculates the distribution of trip duration time - net, excluding stops.
Outputs the results to the output directory (parametrized) in csv format

 
```
├── output
   └── time_net_dist_2021-10-24.csv
      └── part-00000-c4f9ca61-14e1-4279-be57-295e61e4eca4-c000.csv
```
the CSV file contains the following structure :
```
    range_minutes,percentage_of_total
    >= 2hours,100.0
```
 
## Running the ETL job
```
python3 etl/etl.py --input_dir /usr/src/myapp/test/data --output_dir /usr/src/myapp/output --spark_wh_dir /usr/src/myapp/spark_wh --drop_partition_if_exists yes
```

##Build docker image
```
docker build -t geolife .
```
##Run docker image 
```
docker run -it geolife
```

##Running tests 
```
python -m unittest
```

##Notes about the Geolife dataset
some of users folders contain non consistent data about altitude (not integer as most of other users data)
It caused  