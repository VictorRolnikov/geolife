import datetime

from argparse import ArgumentParser
from functools import reduce

from pyspark.sql import DataFrame
from pyspark.sql.types import *
from pyspark.sql.functions import lit, input_file_name


import os

import spark.spark as spark

class Ingester(object):
    """
    Class responsible for retrieving geolife data
    """
    def __init__(self, input_dir, spark_wh_dir, drop_partition_if_exists):

        self.spark_sql_warehouse_dir = spark_wh_dir
        self.folder = input_dir

        self.subfiles_mask = input_dir + "/{subfolder}/Trajectory/*.plt"

        self.ingest_date = datetime.date.today() #+ datetime.timedelta(days=1)

        self.geolife_table_name         = "geolife"
        self.geolife_stg_table_name     = "geolife_stg"
        self.geolife_tempview           = "geolife_new_partition_tempview"
        self.drop_partition_if_exists   = drop_partition_if_exists

    def create_table(self):
        """
        create tables, if not exits
        - geolife_stg - staging table for enrichment and transformation of original data
            ( such as combined timestamp, degries to radinas, co )
        - geolife - optimized data for reports
            ( contains prev state of coordinates and timestamp )
        :return:
        """
        self.session.sql(f"""CREATE TABLE IF NOT EXISTS {self.geolife_stg_table_name} 
                                (user STRING,
                                trajectory STRING,
                                dataset INT,
                                altitude INT,
                                latitude FLOAT, 
                                longitude FLOAT,
                                lat_rad FLOAT, 
                                lon_rad FLOAT,
                                time_stamp TIMESTAMP ) 
                            USING parquet 
                            PARTITIONED BY ( ingest_date TIMESTAMP );""")

        self.session.sql(f"""CREATE TABLE IF NOT EXISTS {self.geolife_table_name} 
                                (user STRING,
                                trajectory STRING,
                                dataset INT,
                                altitude INT,
                                latitude FLOAT, 
                                longitude FLOAT,
                                lat_rad FLOAT, 
                                lon_rad FLOAT,
                                lat_rad_prev FLOAT, 
                                lon_rad_prev FLOAT,
                                time_stamp TIMESTAMP,
                                time_stamp_prev TIMESTAMP ) 
                            USING parquet 
                            PARTITIONED BY ( ingest_date TIMESTAMP );""").show(truncate=False)


    def create_df_schema(self):
        return StructType([StructField("latitude", FloatType()),
                         StructField("longitude", FloatType()),
                         StructField("dataset", IntegerType()),
                         StructField("altitude", IntegerType()),
                         StructField("days", FloatType()),
                         StructField("date", StringType()),
                         StructField("time", StringType()) ])

    def ingest(self):
        subfolders = os.listdir(self.folder)
        df_list = []
        for sf in subfolders:
            path = self.subfiles_mask.format(subfolder=sf)
            df = self.session.read \
                .format('csv') \
                .option('mode', 'DROPMALFORMED') \
                .schema(self.schema) \
                .csv( path ) \
                .withColumn("user", lit(sf)) \
                .withColumn("trajectory", input_file_name() )
            df_list.append(df)

        # .withColumn("trajectory", os.path.basename(str(input_file_name())))
        # .withColumn("trajectory", os.path.basename(input_file_name().cast(StringType())))

        df_all_users = reduce(DataFrame.unionAll, df_list)
        count = df_all_users.count()
        print(f"df_all_users size : {count}")
        df_all_users.createOrReplaceTempView(self.geolife_tempview)

        return count

    def drop_partition(self):

        drop_partition_if_exists_sql = f"""ALTER TABLE {self.geolife_table_name} 
                                            DROP IF EXISTS PARTITION (ingest_date='{self.ingest_date} 00:00:00')"""
        self.session.sql(drop_partition_if_exists_sql)

    def save_spark_table(self):

        insert_tempview_to_stg_sql = f"""INSERT OVERWRITE TABLE {self.geolife_stg_table_name}
                           PARTITION (ingest_date = '{self.ingest_date}') 
                           SELECT   user ,
                                    substring_index(trajectory, '/', -1) as trajectory, 
                                    dataset ,
                                    altitude ,
                                    latitude , 
                                    longitude ,
                                    (latitude * PI() / 180  ) as lat_rad, 
                                    (longitude * PI() / 180) as lon_rad,                                
                                    to_timestamp(CONCAT( date ,' ', time ) ) as time_stamp                                   
                            FROM {self.geolife_tempview} 
                            WHERE dataset IS NOT NULL """

        insert_stg_to_main_sql = f"""INSERT OVERWRITE TABLE {self.geolife_table_name}
                           PARTITION (ingest_date = '{self.ingest_date}') 
                           SELECT   user ,
                                    trajectory,
                                    dataset ,
                                    altitude ,
                                    latitude , 
                                    longitude ,
                                    lat_rad, 
                                    lon_rad,
                                LAG(lat_rad, 1, lat_rad) OVER ( PARTITION BY ingest_date, user, trajectory ORDER BY time_stamp )  as lat_rad_prev,
                                LAG(lon_rad,1, lon_rad) OVER ( PARTITION BY ingest_date, user, trajectory ORDER BY time_stamp )  as lon_rad_prev,                                
                                    time_stamp,                                   
                                LAG(time_stamp,1, time_stamp) OVER ( PARTITION BY ingest_date, user, trajectory ORDER BY time_stamp )  as time_stamp_prev                              
                            FROM {self.geolife_stg_table_name}"""

        self.session.sql(insert_tempview_to_stg_sql)

        self.session.sql(insert_stg_to_main_sql)

        # truncate staging table
        self.session.sql(f"""TRUNCATE TABLE {self.geolife_stg_table_name}""")


    def run(self):

        count = 0

        try:

            self.session = spark.get_or_create_session(self.spark_sql_warehouse_dir)

            self.schema = self.create_df_schema()

            self.create_table()

            if self.drop_partition_if_exists:
                self.drop_partition()

            count = self.ingest()

            self.save_spark_table()

        except Exception as err:
            err = 'Error has occurred {0}'.format(str(err))
            print(err)
            count = -1

        return count


if __name__ == "__main__":

    def parse_arg_command():
        parser = ArgumentParser()
        parser.add_argument("--drop_partition_if_exists", default="yes", required=False, help="yes/no flag to drop ingest date partition if it exists")
        parser.add_argument("--input_dir", default="", required=True, help="landing directory")
        parser.add_argument("--spark_wh_dir", default="", required=True, help="Spark warehouse directory")
        return parser.parse_args()

    args = parse_arg_command()
    input_dir, spark_wh_dir, drop_partition_if_exists = args.input_dir, args.spark_wh_dir, args.drop_partition_if_exists == 'yes'
    if not (os.path.isdir(input_dir) and os.path.exists(os.path.join(input_dir))):
        print(f'the "{input_dir}" input folder doesn\'t exist')
        exit(-1)
    if not spark_wh_dir:
        print('the spark warehouse folder name is empty ')
        exit(-1)

    ingest_obj = Ingester(input_dir, spark_wh_dir, drop_partition_if_exists)

    ingest_obj.run()
