import datetime
from argparse import ArgumentParser

from spark.spark import get_or_create_session

def calc_time_gross_dist(spark_wh_dir, output_dir):
    try:
        session = get_or_create_session(spark_wh_dir)
        report_date = datetime.date.today()
        output_file_name = output_dir + f'\\time_gross_dist_{report_date}.csv'

        session.sql(f"""WITH sampling_diffs as ( SELECT user, trajectory, time_stamp
        										FROM geolife
        										WHERE ingest_date = '{report_date}' 
        										ORDER BY user, trajectory, time_stamp),

        				trip_durations AS ( SELECT user, 
        									MIN(time_stamp) trip_start_time, 
        									MAX(time_stamp) trip_end_time ,
        									((bigint(to_timestamp(MAX(time_stamp))))-(bigint(to_timestamp(MIN(time_stamp)))))/(60) as trip_duration_minutes
        								FROM sampling_diffs
        								GROUP BY user, trajectory) ,

        				ranges AS ( SELECT 0 as from_value, 1 to_value, '<= 1 minute' as title UNION ALL 
        							SELECT 2, 60, '2 min - 6 hours' UNION ALL 
        							SELECT 61, 360, '1 hour - 6 hours' UNION ALL 
        							SELECT 361, 720, '6 hours - 12 hours' UNION ALL 
        							SELECT 721, 1440, '12 hours - 24 hours' UNION ALL 
        							SELECT 1441, 1000000, '>= 24hours')

        				SELECT title as range_minutes,
        						ROUND ( ( COUNT(*) / TT.total_trips ) * 100 , 2) as percentage_of_total
        				FROM ranges 
        					INNER JOIN trip_durations as TD ON TD.trip_duration_minutes >= ranges.from_value AND TD.trip_duration_minutes <= ranges.to_value 
        					CROSS JOIN ( SELECT COUNT(*) total_trips FROM trip_durations ) as TT 
        				GROUP BY from_value, title, total_trips
        				ORDER BY from_value
        ;""").coalesce(1).write\
            .mode('overwrite')\
            .option('header', 'true') \
            .csv(output_file_name)

    except Exception as err:
        err = 'Error has occurred {0}'.format(str(err))
        print(err)
        exit(-1)

    return output_file_name

if __name__ == "__main__":

    def parse_arg_command():
        parser = ArgumentParser()
        parser.add_argument("--spark_wh_dir", default="", required=False, help="Spark warehouse directory")
        parser.add_argument("--output_dir", default="", required=False, help="Output directory for calculated results")

        return parser.parse_args()

    args = parse_arg_command()
    spark_wh_dir, output_dir = args.spark_wh_dir, args.output_dir
    if not spark_wh_dir:
        print('the spark warehouse folder name parameter is missing ')
        exit(-1)

    if not output_dir:
        print('the output folder name parameter is missing ')
        exit(-1)

    calc_time_gross_dist(spark_wh_dir, output_dir)