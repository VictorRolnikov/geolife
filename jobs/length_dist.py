import datetime
from argparse import ArgumentParser

from spark.spark import get_or_create_session

def calc_length_dist(spark_wh_dir, output_dir):
    try:
        session = get_or_create_session(spark_wh_dir)
        report_date = datetime.date.today()
        output_file_name = output_dir + f'\\length_dist_{report_date}.csv'

        session.sql(f"""WITH sampling_distances as ( SELECT user, time_stamp, trajectory, 
											ROUND( 2*6378*asin(sqrt(pow(sin((lat_rad - lat_rad_prev)/2),2) + cos(lat_rad_prev)*cos(lat_rad)*pow(sin((lon_rad-lon_rad_prev)/2),2))), 6) as distance 
                                                    FROM geolife
                                                    WHERE ingest_date = '{report_date}' 
                                                    ),
				
				trip_distances AS ( SELECT user, trajectory, 
                                        ROUND(SUM(distance), 3) as trip_distance
                                    FROM sampling_distances
                                    GROUP BY user, trajectory
                                    ORDER BY user, trajectory) ,
				
				ranges AS ( SELECT 0 as from_value, 5 to_value, '< 5km' as title UNION ALL 
							SELECT 6, 20, '5km - 20km' UNION ALL 
							SELECT 21, 100, '20km - 100km' UNION ALL 
							SELECT 101, 100000, '>= 100km')
							
				SELECT title as range_km,
						ROUND ( ( COUNT(*) / TT.total_trips ) * 100 , 2) as percentage_of_total
				FROM ranges 
					INNER JOIN trip_distances as TD ON TD.trip_distance >= ranges.from_value AND TD.trip_distance <= ranges.to_value 
					CROSS JOIN ( SELECT COUNT(*) total_trips FROM trip_distances ) as TT 
				GROUP BY from_value, title, total_trips
				ORDER BY from_value
				;""").coalesce(1).write\
            .mode('overwrite')\
            .option('header', 'true') \
            .csv(output_file_name)

    except Exception as err:
        err = 'Error has occurred {0}'.format(str(err))
        print(err)
        exit(-1)

    return output_file_name


if __name__ == "__main__":

    def parse_arg_command():
        parser = ArgumentParser()
        parser.add_argument("--spark_wh_dir", default="", required=False, help="Spark warehouse directory")
        parser.add_argument("--output_dir", default="", required=False, help="Output directory for calculated results")

        return parser.parse_args()

    args = parse_arg_command()
    spark_wh_dir, output_dir = args.spark_wh_dir, args.output_dir
    if not spark_wh_dir:
        print('the spark warehouse folder name parameter is missing ')
        exit(-1)

    if not output_dir:
        print('the output folder name parameter is missing ')
        exit(-1)

    calc_length_dist(spark_wh_dir, output_dir)